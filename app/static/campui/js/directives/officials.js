
app = angular.module('campui')




app.directive('outings', ['c2c', 'currentUser', "locale", function (c2c, currentUser, locale) {
    return {
        restrict: 'E',
        scope: {"query":"=?", "data":"=?", "display": "="},
        templateUrl: 'static/campui/views/components/outings.html',
        link: function(scope, element, attrs) {

            scope.getLocale = function(item){ return locale.get(item)}
            scope.currentUser = currentUser

            if(!attrs.data)
                scope.$watch(attrs.query, function(query){
                    scope.data = c2c.outings.get({query:query})
                });
        }
    }
}])

app.directive('images', ['c2c',  "locale", function (c2c,  locale) {
    return {
        restrict: 'E',
        scope: {"query":"=?", "data":"=?", "display": "="},
        template: '<gallery images="data.documents"></gallery>',
        link: function(scope, element, attrs) {
            if(!attrs.data)
                scope.$watch(attrs.query, function(query){
                    scope.data = c2c.images.get({query:query})
                });
        }
    }
}])
