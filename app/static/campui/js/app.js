
(function () {
    angular.module('campui', [
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                 // Ui Bootstrap
        'ui.grid',                      // ui-grid
        'ui.grid.autoResize',
        'ui.grid.infiniteScroll',
        'ui.grid.exporter',
        'ngCookies',
        'ngResource',
        'angular.filter',
        'ngSanitize',
        'gettext',
        'ngMap',
        'tmh.dynamicLocale',
        'slickCarousel',
        'ngPhotoswipe',
        'ui.select',
        'rzModule',
        'infinite-scroll',
    ])
})();
