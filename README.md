## Install :rocket:

1. Install [Node.js](https://nodejs.org/en/)
2. [Download campui](https://gitlab.com/cbeauchesne/campui/-/archive/master/campui-master.zip) and unzip it 
3. In unzipped folder, double-click on file called `CAMPUI_{your-OS}`. The very first time may take a while. **[Et voila!](http://localhost:3000)**
 
## Hack
Modify whatever on `app/`. Changes are applied immediatly.


