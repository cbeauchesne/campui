
var express = require('express')

var app = express()

port = process.env.PORT || 3000

app.use('/static', express.static(__dirname + '/dist/static'));
app.use('/node_modules', express.static(__dirname + '/node_modules'));


app.get('/', function(req, res){
    res.set('Content-Type', 'text/html')
        .sendFile(__dirname + '/dist/index.html');
});

app.listen(port, function () {
  console.log('CampUI app listening on port ' + port )
})